package encryptor;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class EncryptorTest {

    @Test
    public void testSomething() {

        Encryptor encryptor = new Encryptor();
        String hello_ludo = encryptor.cryptSentence("Hello Ludo");
        assertEquals("Jgnnq\"Nwfq", hello_ludo);
    }


}
