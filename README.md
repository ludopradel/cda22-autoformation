# 1/2 journée auto-formation
# Encryptor-Refactoring-Kata


Cette demi-journée peut être faite seule ou en binome.

L'objectif n'est pas de tout terminer, mais d'avoir le code le plus "clean" possible, 
Ca n'est pas grave de ne pas finir.

Concentrez vous sur les 4 rules of Simple Design, écrivez des tests, faites attention au nommage, à la duplication...

## Récupération du repo

```
git clone https://gitlab.com/ludopradel/cda22-autoformation
```

## Création d'une branche 

```
git checkout -b nom.prenom
```

## Commits

Ajoutez des commits aussi souvent que nécessaire, pour me montrer le code intermédiaire.

Le message du commit doit être clair (pour vous et pour les personnes qui vont le relire).

Lorsque vous faites un commit, les tests sont aux verts.

## Push

Pensez à push votre code dans la matinée, et surtout en fin de matinée, lorsque vous avez terminé.

```
git push origin nom-prenom
```

## Sujet

Dans cet exercice (kata), vous allez devoir améliorer le code fourni, en suivant les bonnes pratiques vues en cours.


### Avant de commencer

* Faites une étape à la fois, avancez pas à pas (baby steps).

### Etape 1 : Tests

En règle générale, on ne modifie pas du code de production avant de s'assurer que celui-ci soit couvert par des tests.

S'il n'y a pas de tests, il faut en écrire !

* Dans cette première étape, je vous propose de réfléchir à plusieurs cas d'utilisation pour chaque méthode de la classe `Encryptor` (au moins 1 cas nominal et plusieurs cas aux limites).
* Vous pouvez maintenant écrire les tests correspondants à ces cas d'utilisations.
* Pensez à commiter lorsque vous avez écrit les tests associés à une méthode, pour améliorer la lisibilité de votre historique de commit.

### Tips
Comment tester la méthode `printWords` qui ne retour pas de valeur ? 
  * avons-nous la main sur un élément qui viendrait à changer dans cette méthode ? 
  * qu'est-ce qu'un `PrintStream` (regarder la javadoc associé)
  
Regarder du côté des librairies `assertj` et `hamcrest` pour tester des retours d'exceptions.


### Etape 2 : Refactoring

* Maintenant que vous avez écrit 3 ou 4 tests, pour chacune des méthodes : 
  * `String cryptSentence(String sentence)`
  * `void printWords(String sentence, PrintStream stream)`
  * `String cryptWord(String word)`
  * `String cryptWordToNumbers(String word)`
  * `String cryptWord(String word, String charsToReplace)`
  * `String[] getWords(String sentence)`

vous pouvez commencer à modifier le code source de l'application, sans avoir peur d'introduire de régression dans celui-ci.

Pensez aux 4 règles de design simple :  
* Passes the tests
* Reveals intention
* No duplication
* Fewest elements

En vrac, je peux vous proposer de regarder : 
* le nommage (des variables, des méthodes, ...).
* la duplication (de code, ou duplication de connaissance).
* pouvons-nous déplacer certains types primitifs dans des concepts métiers (pensez à la classe `Cell` dans le cours).
* ...
